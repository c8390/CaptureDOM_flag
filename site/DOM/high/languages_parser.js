function get_languages() {

    fetch('https://gist.githubusercontent.com/jrnk/8eb57b065ea0b098d571/raw/936a6f652ebddbe19b1d100a60eedea3652ccca6/ISO-639-1-language.json', {method:'get'})
        .then(res => res.json())
        .then((res) => {
            for (let country of res)
            {
                let sel = document.getElementById('languages_list');
                let opt = document.createElement('option');
                opt.appendChild( document.createTextNode(country.name) );
                opt.value = country.name;
                sel.appendChild(opt);
            }
        });
}