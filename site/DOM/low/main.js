function get_cars () {

    fetch('https://gist.githubusercontent.com/shcyiza/71c64a33f3880e58980003c4c794db38/raw/febb04707f6ccc9ae3a445e147c5754e30f743fe/car_brands.json')
    .then(res => res.json())
    .then((res) => {
        for (let car of res)
        {
          var sel = document.getElementById('cars_list');
          console.log(car.name);
          let opt = document.createElement('option');
          opt.appendChild( document.createTextNode(car.name) );
          opt.value = car.name;
          sel.appendChild(opt);
        }
    });
}