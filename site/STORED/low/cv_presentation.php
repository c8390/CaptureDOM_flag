<html>
<head>
    <meta charset="UTF-8">
   <title>Capture DOM - STORED Level 1</title>

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="../../static/favicon/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../static/favicon/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../static/favicon/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../static/favicon/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="../../static/favicon/apple-touch-icon-60x60.png" />
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="../../static/favicon/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="../../static/favicon/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="../../static/favicon/apple-touch-icon-152x152.png" />
    <link rel="icon" type="image/png" href="../../static/favicon/favicon-196x196.png" sizes="196x196" />
    <link rel="icon" type="image/png" href="../../static/favicon/favicon-96x96.png" sizes="96x96" />
    <link rel="icon" type="image/png" href="../../static/favicon/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="../../static/favicon/favicon-16x16.png" sizes="16x16" />
    <link rel="icon" type="image/png" href="../../static/favicon/favicon-128.png" sizes="128x128" />
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF" />
    <meta name="msapplication-TileImage" content="mstile-144x144.png" />
    <meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
    <meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
    <meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
    <meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="../../style/level-template.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="/">CapturedDOM</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">HOME</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../DOM/dom.html">DOM</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../REFLECTED/reflected.html">REFLECTED</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../STORED/stored.html">STORED</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../setting.php">SETTINGS</a>
            </li>
        </ul>
    </div>
</nav>
<main role="main" class="container">
    <div class="align-items-start">
        <div class="col text-center">
            <h2>Level 1</h2>
        </div>
        <div class="col">
            <form name="XSS" method="POST" class="text-center">
                <p>Insert your name</p>
                <input style="margin-bottom: 1em;" type="text" name="txtName" id="txtName" >
                <p>Talk about yourself</p>
                <textarea style="margin-bottom: 1em;" type="text" name="mtxMessage" id="mtxMessage" rows="4" cols="50"> </textarea>
                <br>
                <input type="submit" name="btnSign" value="Send" class="btn btn-primary">
                <input type="reset" value="Cancel" class="btn btn-primary">
            </form>
        </div>
        <div class="col text-center" style="margin-top: 8em">
            <a href="../../STORED/stored.html" class="btn btn-primary">Go to STORED page</a>
            <a href="../medium/cv_presentation.php" class="btn btn-primary">Go to next level</a>
        </div>
        </div>
</main>
<footer class="text-center text-white fixed-bottom bg-dark">
    <!-- Grid container -->
    <div class="container p-4"></div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3 bg-dark">
        Stefano Fagnano © 2022 Copyright:
    </div>
    <!-- Copyright -->
</footer>
</body>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
</html>
<?php

$servername = "capture_dom_mysql";
$username = "root";
$password = "wertyuytewfefwgget";

$connection = mysqli_connect($servername, $username, $password);

if( isset( $_POST[ 'btnSign' ] ) ) {
    // Get input
    $message = trim( $_POST[ 'mtxMessage' ] );
    $name    = trim( $_POST[ 'txtName' ] );

    // Sanitize message input
    $message = stripslashes( $message );
    $message = ((is_object($connection)) ? mysqli_real_escape_string($connection,  $message ) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));

    // Sanitize name input
    $name = ((is_object($connection)) ? mysqli_real_escape_string($connection,  $name ) : ((trigger_error("[MySQLConverterToo] Fix the mysql_escape_string() call! This code does not work.", E_USER_ERROR)) ? "" : ""));

    // Update database

    $query  = "INSERT INTO capture_dom.guestbook ( name, message ) VALUES ( '$name', '$message' );";
    $result = mysqli_query($connection,  $query ) or die( '<pre>' . ((is_object($connection)) ? mysqli_error($GLOBALS["___mysqli_ston"]) : (($___mysqli_res = mysqli_connect_error()) ? $___mysqli_res : false)) . '</pre>' );

    //mysql_close();

    $query2 = "SELECT * FROM capture_dom.guestbook";
    $result = $connection->query($query2);
    while($row = $result->fetch_assoc())
    {
        echo '<div class="card"><div class="card-body">';
        echo "<p class='card-text'><strong>Name</strong> ".$row['name']."</p>";
        echo "<p class='card-text'><strong>About</strong> ".$row['message']."</p>";
        echo "</div></div>";
    }

}

?>
